import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { dasherize } from '@ember/string';
import { schedule, later } from '@ember/runloop';

export default class InventoryAddController extends Controller {
	@tracked mathErrorCost = null;
	@tracked mathErrorGT = null;
	@tracked orderedItemTypes = [''];
	@tracked availableItemTypes = [];
	@tracked orderedItems = [''];
	newItemTypeModal = null;
	addNewItemVarietiesModal = null;
	orderDate = null;
	orderId = null;
	formName = "add-inventory-order-form";
	@tracked createdOrder = null;

	@action itemTypeSelected(e){
		let value = e.currentTarget.value.trim();
		let index = e.currentTarget.dataset.itemTypeIndex;
		this.store.findRecord('itemType', value).then(itemType => {
			this.orderedItemTypes[index] = { 
				id: value, 
				name: itemType.name,
				itemType,
				variety: '', 
				availableVarieties: itemType.varieties
			};
			this.orderedItemTypes = this.orderedItemTypes;
			schedule('afterRender', this, function(){
				document.getElementById(`item-variety-${index}`).focus();
			});
		});
	}
	@action itemVarietySelected(e){
		let index = e.currentTarget.dataset.itemTypeIndex;
		let value = document.getElementById(`item-variety-${index}`).value.trim();
		if (this.orderedItems[index]?.varietyName){
			this.orderedItems[index].varietyName = value;
		} else {
			this.orderedItems[index] = { 
				varietyName: value,
				quantity: 0,
				totalCost: null
			};
		}
		//let copy = this.orderedItemTypes[index]
		this.orderedItems = this.orderedItems;
	}
	@action addAnotherItem(){
		this.orderedItemTypes.push('');
		this.orderedItemTypes = this.orderedItemTypes;
	}
	@action quantityChanged(e){
		let el = e.currentTarget;
		this.orderedItems[el.dataset.itemTypeIndex].quantity = parseInt(el.value.trim());
		this.orderedItems = this.orderedItems;
	}
	@action itemCostChanged(e){
		let el = e.currentTarget;
		this.orderedItems[el.dataset.itemTypeIndex].totalCost = parseFloat(el.value.trim());
		this.orderedItems = this.orderedItems;
	}
	@action removeItemFromOrder(e){
		let index = e.currentTarget.dataset.itemTypeIndex;
		this.orderedItemTypes.splice(index, 1);
		this.orderedItems.splice(index, 1);
		schedule('afterRender', this, function(){
			let i = index;
			if (i == this.orderedItems.length){
				i -= 1;
			}
			document.getElementById(`item-type-${i}`).focus();
		});
		this.orderedItemTypes = this.orderedItemTypes;
	}
	@action addVarietyToOrder(itemTypeIndex){
		this.orderedItemTypes[itemTypeIndex].varieties.push('');
		this.orderedItemTypes = this.orderedItemTypes;
	}
	@action updateVarieties(itemTypeIndex){
		let itemType = this.orderedItemTypes[itemTypeIndex];
		for(let variety of this.newVarieties){
			varieties.push({
				varietyName: variety,
				discontinued: false,
				backorders: 0
			});
		}
		itemType.save();
		this.orderedItemTypes[itemTypeIndex] = itemType;
		this.newVarieties = [''];
		this.addNewItemVarietiesModal.hide();
		this.orderedItemTypes = this.orderedItemTypes;
	}

	@tracked newVarieties = [''];
	@tracked newItemName = null;
	@tracked newItemRetailValue = null;
	get varietyZeroBaseCount(){
		return this.newVarieties.length - 1;
	}

	@action toggleAddExisting(){
		this.addToExisting = !this.addToExisting;
	}
	@action addVariety(){
		this.newVarieties.push('');
		schedule('afterRender', this, function(){
			document.getElementById(`variety-${this.newVarieties.length - 1}`).focus();
		});
		this.newVarieties = this.newVarieties;
	}
	@action removeVariety(index){
		this.newVarieties.splice(index, 1);
		this.newVarieties = this.newVarieties;
	}
	@action createItemType(){
		let itemType = this.store.createRecord('itemType', {
			id: dasherize(this.newItemName),
			name: this.newItemName,
			retailValue: [{value: this.newItemRetailValue, date: Date.now()}]
		});
		for(let variety of this.newVarieties){
			if (!variety) continue;
			itemType.addNewVariety(variety);
		}
		itemType.save();
		if (typeof this.availableItemTypes?.push === 'function'){
			this.availableItemTypes.push(itemType);
		} else {
			this.availableItemTypes = [itemType];
		}
		this.availableItemTypes = this.availableItemTypes;
		this.newItemName = null;
		this.newItemRetailValue = null;
		this.newVarieties = [''];
		this.newItemTypeModal.hide();
		let order = { 
			id: dasherize(itemType.name), 
			name: itemType.name,
			itemType,
			variety: '', 
			availableVarieties: itemType.varieties
		};
		let ref = this.orderedItemTypes.length - 1;
		if (this.orderedItemTypes[ref]){
			this.orderedItemTypes.push(order)
			later(this, this.focusOnVariety, ref + 1, 500);
		} else {
			this.orderedItemTypes[ref] = order;
			later(this, this.focusOnVariety, ref, 500);
		}
		this.orderedItemTypes = this.orderedItemTypes;

	}
	focusOnVariety(itemTypeIndex){
		document.getElementById(`item-variety-${itemTypeIndex}`).focus();
	}

	businessSupplies = null;
	shipping = null;
	discounts = null;
	tax = null;
	donations = null;
	grandTotal = null;

	@action addOrderToInventory(e){
		e.preventDefault();
		this.mathErrorGT = null;
		this.mathErrorCost = null;

		let bs = this.businessSupplies ? parseFloat(this.businessSupplies.replace(',', '.')) : 0;
		let s = parseFloat(this.shipping.replace(',', '.'));
		let dis = this.discounts ? parseFloat(this.discounts.replace(',', '.')) : 0;
		let t = parseFloat(this.tax.replace(',', '.'));
		let don = this.donations ? parseFloat(this.donations.replace(',', '.')) : 0;
		let gt = parseFloat(this.grandTotal.replace(',', '.'));
		let cost = bs + s + t + don - dis;
		let itemCost = 0;
		let itemLength = 0;
		for(let item of this.orderedItems){
			itemCost += parseFloat(item.totalCost);
			itemLength += item.quantity;
		}
		cost += itemCost;
		if (cost != gt){
			this.mathErrorCost = cost;
			this.mathErrorGT = gt;
			return;
		}

		let order = this.store.createRecord('inventoryOrder', {
			id: this.orderId,
			businessSupplies: bs,
			additionalDiscounts: dis,
			shipping: s,
			taxes: t,
			donations: don,
			grandTotal: gt,
			orderDate: this.orderDate
		});
		order.save();
		this.createdOrder = order;

		// This provides approximate shipping; doesn't account for business supplies
		let dividedShipping = s / itemLength;
		let taxRate = t / (itemCost + bs);
		let dividedTax = itemCost * taxRate / itemLength;
		for(let i = 0; i < this.orderedItems.length; i++){
			let oi = this.orderedItems[i];
			let it = this.orderedItemTypes[i];
			for(let j = 0; j < oi.quantity; j++){
				let item = this.store.createRecord('item', {
					costInv: oi.totalCost / oi.quantity,
					shippingInv: dividedShipping,
					taxInv: dividedTax,
					dateAcquired: this.orderDate,
					varietyName: oi.varietyName,
					itemType: it.itemType,
					inventoryOrder: order
				});
				item.save();
				
				it.itemType.addNewVariety(item.varietyName);
				it.itemType.save();
			}
		}
		schedule('afterRender', () => document.getElementById('order-link').focus());
		this.clearForm();
	}
	clearForm(){
		this.orderedItemTypes = [''];
		this.orderedItems = [''];
		let form = document.querySelector(`form[name="${this.formName}"]`);
		form.reset();
		for(let el of form.elements){
			if (el.nodeName == 'SELECT') el.selectedIndex = 0;
		}
	}
}
