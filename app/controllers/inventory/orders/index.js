import Controller from '@ember/controller';
import { sort } from '@ember/object/computed';

export default class InventoryOrdersIndexController extends Controller {
	@sort('model', function(a, b){
		let aDate = a.orderTimestamp;
		let bDate = b.orderTimestamp;
		return bDate - aDate;
	}) dateSorted;
}
