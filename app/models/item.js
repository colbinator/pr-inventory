import Model, { attr, belongsTo } from '@ember-data/model';

export default class ItemModel extends Model {
	@attr costInv;
	@attr shippingInv;
	@attr taxInv;
	@attr dateAcquired;
	@attr purchaseValue;
	// Suggested shipping cost to charge client
	@attr({defaultValue: 8}) purchaseShipping;
	@attr purchaseTax;
	// What it actually cost consultant to ship to client
	@attr({defaultValue: 0}) purchaseActualShipping;
	// Must match in itemType.varieties
	@attr varietyName;
	@belongsTo('item-type', {inverse: 'items'}) itemType;
	@belongsTo('purchase-order', {inverse: 'items'}) purchaseoOrder;
	@belongsTo('inventory-order', {inverse: 'items'}) inventoryOrder;

	get costTotal() {
		return (
			this.costInv +
			this.shippingInv +
			this.taxInv +
			this.purchaseActualShipping
		);
	}

	get purchaseTotal() {
		return this.purchaseValue + this.purchaseShipping + this.purchaseTax;
	}

	get netWorth() {
		return this.purchaseTotal - this.costTotal;
	}
}
