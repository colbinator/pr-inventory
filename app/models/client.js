import Model, { attr, hasMany } from '@ember-data/model';

export default class ClientModel extends Model {
	@attr firstName;
	@attr lastName;
	@attr mailingAddress;
	@attr email;
	@attr phone;
	// [ facebook: link, twitter: link, ... ]
	@attr social;
	@hasMany('order') orders;
}
