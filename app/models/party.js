import Model, { attr, hasMany, belongsTo } from '@ember-data/model';

export default class PartyModel extends Model {
	@attr name;
	@attr date;
	@attr hostessCredit;
	@hasMany('order') orders;
	@belongsTo('client') hostess;
}
