import Model, { attr, hasMany } from '@ember-data/model';
import { all } from 'rsvp';

export default class ItemTypeModel extends Model {
	@attr name;
	// [ {value: x, timestamp: yyyyyyyyyyyyyy } ]
	@attr({defaultValue: () => []}) retailValue;
	@attr description;
	// for flavors, scents, or sizes
	// [ { varietyName: 'adsf', discontinued: false || date, backOrders: 0 } ]
	@attr({defaultValue: () => []}) varieties;
	@hasMany('item', {inverse: 'itemType'}) items;

	outOfStock(varietyName) {
		let status = true;
		if (!varietyName) return status;

		for (let item of items) {
			if (item.varietyName != varietyName) continue;
			status = false;
		}
		return status;
	}

	addNewVariety(varietyName){
		for(let variety of this.varieties){
			if (variety.varietyName === varietyName) return false;
		}
		return this.varieties.push({
			varietyName,
			discontinued: false,
			backOrders: 0
		});
	}

	get inStockVarieties(){
		let inStock = {};
		return this.items.then(items => {
			items.forEach(item => {
				debugger;
				if (inStock.hasOwnProperty(item.varietyName)){
					inStock[item.varietyName].push(item);
				} else {
					inStock[item.varietyName] = [item];
				}
			});
			return inStock;
		});
	}
}