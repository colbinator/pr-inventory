import Model, { attr, hasMany } from '@ember-data/model';

export default class InventoryOrderModel extends Model {
	@hasMany('item', {inverse: 'inventoryOrder'}) items;
	@attr orderDate;
	@attr businessSupplies;
	@attr additionalDiscounts;
	@attr shipping;
	@attr taxes;
	@attr donations;
	@attr grandTotal;

	get date(){
		return new Date(this.orderDate).toLocaleDateString('en', {
			month: 'short',
			day: 'numeric',
			year: 'numeric'
		});
	}
	get orderTimestamp(){
		return new Date(this.orderDate).getTime();
	}
	/* get itemVarieties(){
		if (!this.items?.length) return null;
		let hash = {};
		for(let item of this.items){
			let itemName = item.itemType.itemName;
			if (!hash.hasOwnProperty(itemName)){
				hash[itemName] = [
					{
						variety: item.varietyName,
						quantity: 1
						cost
					}
				]
			}
		}
	} */
}
