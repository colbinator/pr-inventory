import Model, { belongsTo, hasMany } from '@ember-data/model';

export default class PurchaseOrderModel extends Model {
	@hasMany('item', {inverse: 'purchaseOrder'}) items;
	@belongsTo('client') client;
	@belongsTo('party') party;
}
