import Route from '@ember/routing/route';
import { schedule } from '@ember/runloop';
import { action } from '@ember/object';

export default class InventoryAddRoute extends Route {
	model(){
		return this.store.findAll('itemType');
	}
	setupController(controller, model){
		super.setupController(...arguments);
		controller.set('availableItemTypes', model);
		schedule('afterRender', this, function(){
			controller.newItemTypeModal = new window.bootstrap.Modal(document.getElementById('create-new-item-type'));
		});
	}
	@action willTransition(){
		if (this.controller.newItemTypeModal._isShown){
			this.controller.newItemTypeModal.hide();
		}
		return true;
	}
}
