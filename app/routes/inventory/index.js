import Route from '@ember/routing/route';

export default class InventoryIndexRoute extends Route {
	// Show in stock items at a glance
	async model(){
		let itemTypes = await this.store.findAll('item-type', {include: 'items'});
		let inStockTypes = [];
		let inStockItems = {};
		await itemTypes.forEach(async itemType => {
			let items = await itemType.items;
			items.forEach(item => {
				if (!item.purchaseValue){
					if (!inStockTypes.includes(itemType)){
						inStockTypes.push(itemType);
						inStockItems[itemType.name] = {};
						inStockItems[itemType.name][item.varietyName] = [item];
					} else {
						inStockItems[itemType.name][item.varietyName].push(item);
					}
				}
			});
		});
		
		return {inStockTypes, inStockItems};
	}
}
