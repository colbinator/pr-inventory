import Route from '@ember/routing/route';

export default class InventoryOrdersOrderRoute extends Route {
	model(params){
		return this.store.findRecord('inventory-order', params.id);
	}
	setupController(controller, model){
		super.setupController(...arguments);
		let itemTypes = {};
		model.items.then(async items => {
			if (items.length){
				for(let item of items.toArray()){
					let itemType = await item.itemType;
					let iTName = itemType.name;
					if (itemTypes.hasOwnProperty(iTName)){
						if (itemTypes[iTName].hasOwnProperty(item.varietyName)){
							itemTypes[iTName][item.varietyName].items.push(item);
							itemTypes[iTName][item.varietyName].total += item.costInv;
						} else {
							itemTypes[iTName][item.varietyName] = {
								total: item.costInv,
								items: [item]
							};
							itemTypes[iTName].varieties.push(item.varietyName);
						}
					} else {
						let thing = {
							itemType: item.itemType,
							varieties: [item.varietyName]
						};
						thing[item.varietyName] = {
							total: item.costInv,
							items: [item]
						};
						itemTypes[iTName] = thing;
					}
				}
				controller.set('itemTypes', itemTypes);
			}
		});
	}
}
