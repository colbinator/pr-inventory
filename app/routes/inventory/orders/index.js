import Route from '@ember/routing/route';

export default class InventoryOrdersIndexRoute extends Route {
	model(){
		return this.store.findAll('inventory-order');
	}
}
