import EmberRouter from '@ember/routing/router';
import config from 'pr-inv/config/environment';

export default class Router extends EmberRouter {
	location = config.locationType;
	rootURL = config.rootURL;
}

Router.map(function () {
	this.route('inventory', function () {
		this.route('add');
		this.route('orders', function () {
			this.route('order', { path: ':id' });
		});
	});
});
