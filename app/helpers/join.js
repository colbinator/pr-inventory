import { helper } from '@ember/component/helper';

export default helper(function join(stringArr/*, named*/) {
  return stringArr.join(' ');
});
