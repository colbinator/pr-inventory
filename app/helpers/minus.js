import { helper } from '@ember/component/helper';

export default helper(function minus([a, b]/*, named*/) {
  return a - b;
});
