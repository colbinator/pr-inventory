import { helper } from '@ember/component/helper';

export default helper(function or(positional/*, named*/) {
	let result;
	for (let val of positional) {
		if (!result) result = val;
		result = result || val;
	}
	return result;
});
