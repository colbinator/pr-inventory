import { helper } from '@ember/component/helper';

export default helper(function toJson(positional/*, named*/) {
  return JSON.stringify(positional[0]);
});
