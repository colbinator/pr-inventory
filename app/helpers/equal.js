import { helper } from '@ember/component/helper';

export default helper(function equal([a, b]/*, named*/) {
  return a == b;
});
