import Component from '@glimmer/component';
import { inject } from '@ember/service';

export default class NavigationComponent extends Component {
	@inject router;

	get currentRouteName(){
		return this.router.currentRouteName;
	}
}
